import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../models/item';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
  providers: [HttpService]
})
export class MovieComponent {

  @Input() item: Item;

  constructor() {
  }


}
