import { Component, OnInit } from '@angular/core';
import {MovieList} from '../models/movieList';
import {HttpService} from '../http.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
  providers: [HttpService]
})
export class MovieListComponent implements OnInit {
  movieList: MovieList;
  searchMovieList: MovieList;

  constructor(private httpService: HttpService) {
  }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.getData();
  }

  // tslint:disable-next-line:typedef
  search(searchText: string){
    if (isNaN(parseInt(searchText, 10)))
    {
      this.searchByTitle(searchText);
    }
    else {
      this.searchByYear(parseInt(searchText, 10));
    }
  }

  // tslint:disable-next-line:typedef
  searchByTitle(text: string){
    this.searchMovieList.movies = this.movieList.movies.filter(o => o.title.toLowerCase().includes(text.toLowerCase()));
  }

  // tslint:disable-next-line:typedef
  searchByYear(year: number){
    this.searchMovieList.movies = this.movieList.movies.filter(o => o.year === year);
  }

  // tslint:disable-next-line:typedef
  getData(){
    this.httpService.getData().subscribe((data: MovieList) => {this.movieList = data; this.searchMovieList = this.movieList; });
  }
}
