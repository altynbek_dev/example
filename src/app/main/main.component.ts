import {Component, OnInit} from '@angular/core';
import {Item} from '../models/item';
import {HttpService} from '../http.service';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [HttpService]
})
export class MainComponent implements OnInit {

  items: Item[] = [];
  pageOfItems: Array<any>;


  constructor(private httpService: HttpService){}

  // tslint:disable-next-line:typedef
  ngOnInit(){
    this.httpService.getData().subscribe(data => this.items = data["movies"]);
  }

  // tslint:disable-next-line:typedef
  onChangePage(pageOfItems: Array<any>) {
    // update current page of items
    this.pageOfItems = pageOfItems;
  }

  search(text: string): void{
    this.items.filter(o =>
      Object.keys(o).some(k => o[k].toLowerCase().includes(text.toLowerCase())));
  }
}
