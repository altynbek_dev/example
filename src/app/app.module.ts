import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {JwPaginationModule} from 'jw-angular-pagination';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {MainComponent} from './main/main.component';
import {FormComponent} from './form/form.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieComponent } from './movie/movie.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


const appRoutes: Routes = [
  { path: 'form', component: FormComponent},
  { path: '', component: MainComponent},
  { path: 'movieList', component: MovieListComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainComponent,
    FormComponent,
    MovieListComponent,
    MovieComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    JwPaginationModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent,
  ]
})
export class AppModule {
}
