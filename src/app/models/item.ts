export class Item {
  title: string;
  year: number;
  poster: string;
}
